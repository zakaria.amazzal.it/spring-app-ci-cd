FROM openjdk:8-alpine

WORKDIR /app

COPY . /app

RUN apk add maven

RUN chmod 775 /app

EXPOSE 8080

CMD ["java", "-jar", "/app/target/demo-0.0.1-SNAPSHOT.jar"]
